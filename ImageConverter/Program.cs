﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;


namespace ImageConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write ("Put your \"inputImage.jpg\" to the same folder with \"ImageConverter.exe\".");
            Console.Write ("This program will perform convertion of your image to symbolic image \n");
            Console.Write ("Enter desired result height in symbols number (50-100 recommended): ");
            int resolution = Convert.ToInt32 (Console.ReadLine());
            
            Bitmap inputImage = new Bitmap("inputImage.jpg");
            int width = inputImage.Width;
            int height = inputImage.Height; 

            StreamWriter file = new StreamWriter("outputImage.txt");
            for (int i = 0; i < height; i += height / resolution)
            {
                for (int j = 0; j < width; j += (width / resolution) * height / width / 3 * 2)
                {
                    if (inputImage.GetPixel(j, i).B > 200)
                    {
                        file.Write(".");
                    }
                    else if (inputImage.GetPixel(j, i).B > 150)
                    {
                        file.Write("-");
                    }
                    else if (inputImage.GetPixel(j, i).B > 100)
                    {
                        file.Write("1");
                    }
                    else if (inputImage.GetPixel(j, i).B > 50)
                    {
                        file.Write("7");
                    }
                    else
                    {
                        file.Write("8");
                    }
                }
                file.WriteLine();
            }
        }
    }
}
